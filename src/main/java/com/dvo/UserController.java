package com.dvo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Danyl on 020 20.10.17.
 */
@Controller
public class UserController {
    @RequestMapping("/")
    public String get() {
        return "index";
    }

    @RequestMapping("/about")
    public String content(){
        return "about";
    }

}
